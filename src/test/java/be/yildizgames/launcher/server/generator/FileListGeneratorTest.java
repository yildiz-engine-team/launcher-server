/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.server.generator;

import be.yildizgames.common.file.FileResource;
import be.yildizgames.common.os.factory.OperatingSystems;
import be.yildizgames.launcher.server.generator.FileListGenerator;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

final class FileListGeneratorTest {

    private static final String WIN64 = "/win64/";

    private static final String LINUX64 = "/linux64/";

    @Test
    void testFileListGenerator() throws IOException {

        Path folder = Files.createTempDirectory("test");
        FileResource.createDirectory(folder.toString() + WIN64);
        FileResource.createDirectory(folder.toString() + LINUX64);
        FileListGenerator generator = new FileListGenerator(folder);
        generator.buildSystemFiles(OperatingSystems.WIN64.getSystem());
        assertTrue(new File(folder.toString() + WIN64 + "list.xml").exists());
        generator.buildSystemFiles(OperatingSystems.LINUX64.getSystem());
        assertTrue(new File(folder.toString() + LINUX64 + "list.xml").exists());
        assertEquals(new File(folder.toString() + WIN64 + "list.xml").length(), new File(folder.toString() + LINUX64 + "list.xml").length());

        Path folder2 = Files.createTempDirectory("test");
        FileResource.createDirectory(folder2.toString() + WIN64);
        FileResource.createDirectory(folder2.toString() + LINUX64);
        File f = new File(folder2.toString() + WIN64 + "files/test.txt");
        if(!f.getParentFile().mkdirs()) {
            fail("Cannot create directories");
        }
        try (FileWriter fw = new FileWriter(f)) {
            fw.append("a little test");
        }
        generator = new FileListGenerator(folder2);
        generator.buildSystemFiles(OperatingSystems.WIN64.getSystem());
        assertTrue(new File(folder2.toString() + WIN64 + "list.xml").length() > new File(folder2.toString() + LINUX64 + "list.xml").length());
        generator.buildSystemFiles(OperatingSystems.LINUX64.getSystem());
        assertTrue(new File(folder2.toString() + WIN64 + "list.xml").length() > new File(folder2.toString() + LINUX64 + "list.xml").length());
    }
}
