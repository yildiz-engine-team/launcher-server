/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */
package be.yildizgames.launcher.server.webhook;

import be.yildizgames.common.compression.CompressionFactory;
import be.yildizgames.common.logging.LogFactory;
import be.yildizgames.common.os.OperatingSystem;
import be.yildizgames.launcher.server.config.Configuration;
import be.yildizgames.launcher.server.watcher.Watcher;
import be.yildizgames.launcher.server.webhook.client.Client;
import io.undertow.Undertow;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import org.slf4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class WebHookServer extends Watcher {

    private static final Logger LOGGER = LogFactory.getInstance().getLogger(WebHookServer.class);

    private static final String HEADER_TEXT = "text/plain";

    private final Client client;

    private final int port;

    private final String host;

    private final String expectedToken;

    private final Path root;

    private WebHookServer(int httpPort, String host, String expectedToken, Path root, String fileUrl) {
        super(root);
        LOGGER.info("Running web hook server on {}:{} | File storage: {}.",host, httpPort, root.toString());
        this.port = httpPort;
        this.host = host;
        this.expectedToken = expectedToken;
        this.client = new Client(fileUrl);
        this.root = root;
    }

    public static WebHookServer fromConfiguration(Configuration configuration) {
        return new WebHookServer(
                configuration.getHttpPort(),
                configuration.getHttpHost(),
                configuration.getAuthenticationToken(),
                configuration.getRoot(),
                configuration.getFileUrl());
    }

    public void start() {
        LOGGER.info("Starting web hook server.");
        Undertow server = Undertow.builder()
                .addHttpListener(this.port, this.host)
                .setHandler(this::handleQuery)
                .build();
        server.start();
    }

    private void handleQuery(HttpServerExchange exchange) {
        Request request = Request.fromParameters(exchange.getQueryParameters());
        LOGGER.debug(request.toString());
        if(request.isValid(this.expectedToken)) {
            this.respondOk(exchange);
            for(OperatingSystem system : request.getSystems()) {
                try {
                    Path fileToWrite = this.root.resolve(system.getName()).resolve("package.zip");
                    Files.copy(this.client.getFile(system.getName() + ".zip"), fileToWrite, StandardCopyOption.REPLACE_EXISTING);
                    CompressionFactory.zipUnpacker().unpack(fileToWrite, fileToWrite.getParent().resolve("files"), false);
                } catch (IOException | URISyntaxException e) {
                    LOGGER.error("Error copying file", e);
                }
                this.updated(system);
            }
        } else {
            LOGGER.warn("Invalid request: {}", request);
            this.respondUnauthorized(exchange);
        }
    }

    private void respondOk(HttpServerExchange exchange) {
        LOGGER.debug("Sending ok response.");
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, HEADER_TEXT);
        exchange.setStatusCode(200);
        exchange.getResponseSender().send("ok");
    }

    private void respondNotOk(HttpServerExchange exchange) {
        LOGGER.debug("Sending not ok response.");
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, HEADER_TEXT);
        exchange.setStatusCode(500);
        exchange.getResponseSender().send("Your request could not be processed.");
    }

    private void respondUnauthorized(HttpServerExchange exchange) {
        LOGGER.debug("Sending unauthorized response.");
        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, HEADER_TEXT);
        exchange.setStatusCode(401);
        exchange.getResponseSender().send("Your request could not be processed.");
    }
}
