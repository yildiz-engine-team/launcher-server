package be.yildizgames.launcher.server.webhook;

import be.yildizgames.common.os.OperatingSystem;
import be.yildizgames.common.os.factory.OperatingSystems;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Request {

    private String token = "";

    private List<OperatingSystem> systems = new ArrayList<>();

    private Request( Map<String, Deque<String>> parameters) {
        for(Map.Entry<String, Deque<String>> entry : parameters.entrySet()) {
            if(entry.getKey().equals("token")) {
                this.token = entry.getValue().getFirst();
            } else if (entry.getKey().equals("system")) {
                this.systems.addAll(entry.getValue()
                        .stream()
                        .map(String::toUpperCase)
                        .map(OperatingSystems::valueOf)
                        .map(OperatingSystems::getSystem)
                        .collect(Collectors.toSet()));
            }
        }
    }

    static Request fromParameters(Map<String, Deque<String>> parameters) {
        return new Request(parameters);
    }

    boolean isValid(String expected) {
        return expected.equals(this.token);
    }

    List<OperatingSystem> getSystems() {
        return Collections.unmodifiableList(this.systems);
    }

    @Override
    public String toString() {
        return "HTTP request with token: " + token + " for systems " + this.systems;
    }
}
