/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */
package be.yildizgames.launcher.server.files;

import be.yildizgames.common.os.OperatingSystem;
import be.yildizgames.common.os.factory.OperatingSystems;
import be.yildizgames.launcher.server.watcher.Watcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.util.ArrayList;
import java.util.List;

public class AllSystemListGenerator extends Watcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(AllSystemListGenerator.class);

    private final List<OperatingSystem> supportedSystems = List.of(
            OperatingSystems.WIN64.getSystem(),
            OperatingSystems.LINUX64.getSystem());

    private final List<DirectoryWatcher<OperatingSystem>> watchers = new ArrayList<>();

    private boolean running;

    private AllSystemListGenerator(Path root) throws IOException {
        super(root);
        for(OperatingSystem os : supportedSystems) {
            LOGGER.info("Registering system {}.", os.getName());
            watchers.add(DirectoryWatcher.watch(os, root.resolve(os.getName()).resolve("files")));
        }
    }

    public static AllSystemListGenerator inPath(Path root) throws IOException {
        return new AllSystemListGenerator(root);
    }

    @Override
    public void start() {
        this.running = true;
        while(this.running) {
            for(DirectoryWatcher<OperatingSystem> watcher : this.watchers) {
                List<WatchEvent<?>> events = watcher.pollEvents();
                if (!events.isEmpty()) {
                    this.updated(watcher.getId());
                }
            }
        }
    }

    public void stop() {
        this.running = false;
    }

}
