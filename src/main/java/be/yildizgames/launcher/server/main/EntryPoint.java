/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.server.main;

import be.yildizgames.common.git.GitProperties;
import be.yildizgames.common.git.GitPropertiesProvider;
import be.yildizgames.common.logging.LogFactory;
import be.yildizgames.launcher.server.config.Configuration;
import be.yildizgames.launcher.server.webhook.WebHookServer;
import org.slf4j.Logger;

/**
 * Entry point class for the launcher server.
 * 
 * @author Van den Borre Grégory
 */
public final class EntryPoint {

    private static final Logger LOGGER = LogFactory.getInstance().getLogger(EntryPoint.class);

    private EntryPoint() {
        super();
    }

    /**
     * Entry point for the launcher server.
     * 
     * @param args Contains the configuration file path.
     */
    public static void main(String[] args) {
        try {
            GitProperties git = GitPropertiesProvider.getGitProperties();
            LOGGER.info("Starting Yildiz Launcher Server version {} ", git.getCommitId());
            WebHookServer
                    .fromConfiguration(Configuration.fromArgs(args))
                    .start();
        } catch (Exception e) {
            LOGGER.error("Unhandled exception", e);
        }
    }
}
