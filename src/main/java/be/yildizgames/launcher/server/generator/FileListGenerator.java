/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.launcher.server.generator;

import be.yildizgames.common.file.exception.FileCreationException;
import be.yildizgames.common.file.ResourceUtil;
import be.yildizgames.common.logging.LogFactory;
import be.yildizgames.common.os.OperatingSystem;
import be.yildizgames.launcher.shared.constant.Constants;
import be.yildizgames.launcher.shared.files.ListBuilder;
import org.slf4j.Logger;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Can generate the list of files to be delivered.
 * @author Van den Borre Grégory
 */
public final class FileListGenerator {

    private static final Logger LOGGER = LogFactory.getInstance().getLogger(FileListGenerator.class);

    private static final String DIRECTORY = "files";

    private final Path basePath;

    public FileListGenerator(final Path basePath) {
        super();
        if(basePath == null) {
            throw new AssertionError("Base path cannot be null.");
        }
        this.basePath = basePath;
    }

    /**
     * Generate the file list according to a given operating system.
     * 
     *
     * @param system
     *            OS to use.
     *
     */
    //@Requires system != null
    //@Ensures new File(this.basePath + "/" + system.getPath() + "/" + Constants.LIST).exists()
    public void buildSystemFiles(final OperatingSystem system) {
        LOGGER.info("Building file list for system: {}", system);
        Path osSpecific = this.basePath.resolve(system.getName());
        if(!Files.exists(osSpecific)) {
            LOGGER.info("Path {} does not exists, creating it.", osSpecific);
            try {
                Files.createDirectories(osSpecific);
            } catch (IOException e) {
                LOGGER.error("Cannot create directory", e);
                throw new FileCreationException(e);
            }
        }
        Path resourceListFile = osSpecific.resolve(Constants.LIST);
        try (Writer writer = ResourceUtil.getFileWriter(resourceListFile)) {
            ListBuilder builder = new ListBuilder(osSpecific.resolve(DIRECTORY));
            writer.append(builder.createList());
            LOGGER.info("File list built for system {}", system);
        } catch (IOException e) {
            LOGGER.error("Error building file", e);
        }
    }
}
