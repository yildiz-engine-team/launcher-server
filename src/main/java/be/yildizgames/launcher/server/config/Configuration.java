/*
 * This file is part of the Yildiz-Engine project, licenced under the MIT License  (MIT)
 *
 *  Copyright (c) 2018 Grégory Van den Borre
 *
 *  More infos available: https://www.yildiz-games.be
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 *  documentation files (the "Software"), to deal in the Software without restriction, including without
 *  limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 *  of the Software, and to permit persons to whom the Software is furnished to do so,
 *  subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in all copies or substantial
 *  portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 *  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 *  OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 *  DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */
package be.yildizgames.launcher.server.config;

import be.yildizgames.common.file.FileProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class Configuration {

    private static final Logger LOGGER = LoggerFactory.getLogger(Configuration.class);

    private static final String FILE_ROOT = "files.root";

    private static final String HTTP_HOST = "http.host";

    private static final String HTTP_PORT = "http.port";

    private static final String HTTP_TOKEN = "http.token";

    private static final String HTTP_REQUEST = "http.request";

    private final Path root;

    private final int httpPort;

    private final String httpHost;

    private final String authenticationToken;

    private final String fileUrl;

    private Configuration(final Properties properties) {
        this.root = Paths.get(properties.getProperty(FILE_ROOT, "files"));
        this.httpPort = Integer.valueOf(properties.getProperty(HTTP_PORT, "80"));
        String host;
        try {
            host = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            LOGGER.error("Unknown host", e);
            host = "localhost";
        }
        this.httpHost = properties.getProperty(HTTP_HOST, host);
        this.authenticationToken = properties.getProperty(HTTP_TOKEN, "c5da1f2a");
        this.fileUrl = properties.getProperty(HTTP_REQUEST, "https://bitbucket.org/yildiz-engine-team/build-application-binaries/downloads/");
    }

    public static Configuration fromArgs(final String[] args) {
        if (args == null || args.length < 1) {
            LOGGER.warn("No configuration path set, using default");
            return new Configuration(new Properties());
        } else {
            Properties properties = FileProperties.getPropertiesFromFile(Paths.get(args[0]));
            return new Configuration(properties);
        }
    }

    public final Path getRoot() {
        return this.root;
    }

    public final int getHttpPort() {
        return httpPort;
    }

    public final String getAuthenticationToken() {
        return this.authenticationToken;
    }

    public final String getHttpHost() {
        return this.httpHost;
    }

    public String getFileUrl() {
        return this.fileUrl;
    }
}
